'use strict';
const startTime = new Date();
const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');
const nconf = require('nconf');
const path = require('path'); // native nodeJs module for resolving paths // const PORT = process.env.PORT || 3000; //get port
const os = require('os'); // native nodeJs module and info of the platform

const logInfo = msg => {
  process.stdout.write(`${msg}\n`);
  // platformLogger.info(msg);
};

// Setup nconf to use (in-order):
//   1. Command-line arguments
//   2. Environment variables
//   3. A file located at './conf/config.json'

nconf
  .argv()
  .env()
  .file({
    file: `${__dirname}/server/public/config/config.json`
  });

process.env.PROTOCOL = nconf.get('server:protocol') || 'http';
process.env.PORT = process.env.PORT || nconf.get('server:port') || 3000;
process.env.TIMEOUT = parseInt(
  process.env.TIMEOUT || nconf.get('server:timeout') || 0,
  10
);
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.ROOT = path.normalize(__dirname);

const logger = require(path.join(
  process.env.ROOT,
  'server',
  'logger',
  'logger'
));

// database startup initialization
const loadLocalData = nconf.get('startup:loadLocalData');
const overwriteLocalData = nconf.get('startup:overwriteLocalData');
// const loadRemoteData = nconf.get('startup:loadRemoteData');
// const overwriteRemoteData = nconf.get('startup:overwriteRemoteData');
// const databaseProtocol = nconf.get('database:protocol');
// const datebaseHost = nconf.get('database:host');
// const datebasePort = nconf.get('database:port');
// test connection
// const db = require('./server/database/initialization')
// db.getDbConnection(null)

//
const router = require('./server/routes');

const app = express();

// //load static html file for admin part
// app.get('/', (req, res) => res.sendFile('./client/index.html', {
//     root: __dirname
// }))

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// Start server
const httpServer = require(process.env.PROTOCOL).Server(app); // const server = require('http').Server(app);
// Check if httpServer has method setTimeout. Node version < 0.12.0
if (httpServer.setTimeout) {
  httpServer.setTimeout(process.env.TIMEOUT * 1000);
}

// Application routes
router(app);

logInfo('+=====================+');
logInfo('+    ikeyPass Api     +');
logInfo('+=====================+');

// listen on port ====== > make our app listen for incoming requests on the portassigned above
app.listen(process.env.PORT, () => {
  // trap errors when the listen throw an error
  httpServer.on('error', err => {
    if (err.code === 'EADDRINUSE') {
      logError(`Address in use on the port ${process.env.PORT}`);
      logError(`Error: ${err}`);
      process.exit(1);
    } else {
      logError(`Unknown Error when listen the httpServer: ${err}`);
      process.exit(1);
    }
  });
  // loaddata
  require('./server/database/initialization').loadAll(
    loadLocalData,
    overwriteLocalData
  );
  logInfo(`Server runnimg on ${process.env.PORT}`);
  logInfo('Server protocol is %s', process.env.PROTOCOL);
  logInfo(`OS hostname: ${os.hostname()}`);
  logInfo(`OS Type: ${os.type()}`);
  logInfo(`Platform: ${os.platform()}`);
  logInfo(`Release: ${os.release()}`);
  // logInfo('CPUs:' + JSON.stringify(os.cpus()));
  logInfo(`Total Mem:${os.totalmem()} bytes`);
  logInfo(`Free Mem:${os.freemem()} bytes`);
  logger.log('info', 'test message %d', 123);
  // used to comptue start time duration
  const endTime = new Date();
  logInfo(
    `ikeyPass Api started in ${endTime.getTime() - startTime.getTime()} ms...`
  );
});
