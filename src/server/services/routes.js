/*
    SERVICES Routes
*/

const fs = require('fs');
const path = require('path');

const directory = path.join(process.env.ROOT, 'server', 'services');

module.exports = app => {
  console.log('Loading dynamically all REST APIs in (%s)...\n', directory);

  fs.readdirSync(directory).forEach(fileName => {
    // console.log('LISTING : ' + fileName);
    if (fs.statSync(path.join(directory, fileName)).isDirectory()) {
      // require(directory + fileName + '/routes')(app);
      try {
        console.log('fileName loaded', `${fileName}./${fileName}/routes`);
        require(`./${fileName}/routes`)(app);
      } catch (e) {
        // serverLogger.error('Cannot require file: ' + e)//
        console.log(`Cannot require file: ${e}`);
        throw e;
      }
    }
  });
};
