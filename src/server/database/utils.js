
const nconf = require('nconf');
const util = require('util');
const path = require('path');

const defaultDbConf = nconf.get('database');
const mongoose = require('mongoose');

const logger = require(path.join(
  process.env.ROOT,
  'server',
  'logger',
  'logger',
));

exports.getDbConnection = (dbConf) => {
  console.log('getDbConnection');
  dbConf = dbConf || defaultDbConf || {};
  const dbProtocol = dbConf.protocol || 'http';

  const dbUser = dbConf.username && dbConf.password
    ? util.format('%s:%s@', dbConf.username, dbConf.password)
    : '';

  const dbHost = dbConf.host || '127.0.0.1';

  const dbPort = dbConf.port || '5984';

  const url = 'mongodb://localhost:27017/ikeyPass-dev';
  console.log('url --------------------- <w', url);
  const dbCon = mongoose.connect(url, {
    useNewUrlParser: true,
  });

  return dbCon;

  // return mongoose.Promise = global.Promise;
};
