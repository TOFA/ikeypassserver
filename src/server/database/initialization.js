const mongoose = require('mongoose');

const load = async () => {
  console.log(' loaad partial data');
  // get connection
  const db = await require('./utils').getDbConnection();

  console.log(' the db connection ', db);

  const loadedUserAdminData = require('../../data/admin-users/admin-users');

  console.log(' loadedUserAdminData  ====== > ', loadedUserAdminData);

  // get schema
  const userAdminSchema = require('../models/admin-users');
  // creat model user-admin
  const UserAdminModel = mongoose.model('admin-users', userAdminSchema);

  const userAdmin = new UserAdminModel(loadedUserAdminData[0]);

  userAdmin.save((err) => {
    if (err) {
      console.log(' err ', err);
      throw new Error(err);
    } else {
      console.log(' db admin-users ok');
    } // saved!
  });

  // userAdmin.db.collection('admin-users', loadedUserAdminData[0]);


  // userAdmin.findByIdAndUpdate(userID,
  //   { $push: { userAdmin: loadedUserAdminData[0] } },
  //   { safe: true, upsert: true },
  //   (err, doc) => {
  //     if (err) {
  //       console.log(err);
  //     } else {
  //       // do stuff
  //     }
  //   });
};

exports.loadAll = (loadLocalData, overwriteLocalData) => {
  if (loadLocalData && overwriteLocalData) {
    load();
  }
};
