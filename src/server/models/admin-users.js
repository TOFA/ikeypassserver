

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const adminUserSchema = new Schema({
  _id: String,
  user_id: String,
  name: String,
  password: String,
  roles: [],
  description: String,
}, {
  _id: false,
});

module.exports = adminUserSchema;
