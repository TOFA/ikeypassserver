/* 
 winston logger file
 */
const winston = require('winston');

const logger = new winston.Logger({
  level: 'info',
  // format: winston.format.json(),
  transports: [
    new winston.transports.File({
      filename: './logs/debug.log',
      json: false,
      colorize: true
    })
  ],
  exceptionHandlers: [
    new winston.transports.Console({
      json: false,
      timestamp: true
    }),
    new winston.transports.File({
      filename: '../../logs/exceptions.log',
      json: false,
      colorize: true
    })
  ],
  exitOnError: false
});

module.exports = logger;
